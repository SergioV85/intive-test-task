import { Routes } from '@angular/router';

export const ROUTES: Routes = [
  {
    path: 'rates',
    loadChildren: './core/exchange-rates#ExchangeRatesModule'
  },
  {
    path: '',
    redirectTo: '/rates',
    pathMatch: 'full'
  }
];
