import { Component, DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';
import { Observable } from 'rxjs/Observable';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BsModalService, ModalModule } from 'ngx-bootstrap/modal';
import { ExchangeRatesService } from './../../shared/services/exchange-rates/exchange-rates.service';
import { ExchangeRatesComponent } from './exchange-rates.component';

describe('ExchangeRatesComponent', () => {
  let component: ExchangeRatesComponent;
  let fixture: ComponentFixture<ExchangeRatesComponent>;
  let exchangeRatesService, modalService;
  let exchangeRateSpy, modalSpy;

  const mockedRates = [
    { 'Id': 1, 'Date': '2017-11-05T00:00:00', 'CurrencyPair': 'EURUSD', 'Rate': 1.17 },
    { 'Id': 2, 'Date': '2017-11-01T00:00:00', 'CurrencyPair': 'EURCHF', 'Rate': 1.14 },
    { 'Id': 3, 'Date': '2017-10-29T00:00:00', 'CurrencyPair': 'EURUSD', 'Rate': 1.16 },
    { 'Id': 4, 'Date': '2017-11-08T00:00:00', 'CurrencyPair': 'EURCHF', 'Rate': 1.15 },
    { 'Id': 5, 'Date': '2017-11-01T00:00:00', 'CurrencyPair': 'EURCHF', 'Rate': 1.165 }
];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        ModalModule.forRoot()
      ],
      declarations: [
        ExchangeRatesComponent,
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ],
      providers: [
        ExchangeRatesService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExchangeRatesComponent);
    component = fixture.componentInstance;
    exchangeRatesService = fixture.debugElement.injector.get(ExchangeRatesService);
    exchangeRateSpy = spyOn(exchangeRatesService, 'getRates')
      .and.returnValue(Observable.of(mockedRates));

    modalService = fixture.debugElement.injector.get(BsModalService);
    modalSpy = spyOn(modalService, 'show')
      .and.returnValue({ content: {} });

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have title', () => {
    const title = fixture.debugElement.query(By.css('.text-center')).nativeElement;
    expect(title.textContent).toEqual('Currency Rates');
  });

  it('should create a list of rate', () => {
    const listContainer = fixture.debugElement.query(By.css('.list-group'));
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(listContainer.children.length).toBe(5);
    });
  });

  it('should open popup for editing', () => {
    component.editRate(mockedRates[0]);
    expect(modalService.show).toHaveBeenCalled();
  });
});
