import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ExchangeRatesService } from './../../shared/services/exchange-rates/exchange-rates.service';
import { RateEditDialogComponent } from './rate-edit-dialog/rate-edit-dialog.component';

@Component({
  selector: 'app-exchange-rates',
  templateUrl: './exchange-rates.component.html',
  styleUrls: ['./exchange-rates.component.scss']
})
export class ExchangeRatesComponent implements OnInit, OnDestroy {
  public bsModalRef: BsModalRef;
  public rates: App.ExchangeRates.Rate[];
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(private exchangeRatesService: ExchangeRatesService,
              private modalService: BsModalService) { }

  public ngOnInit() {
    this.getRates();
    this.modalService.onHide
      .pipe(
        takeUntil(this.ngUnsubscribe)
      )
      .subscribe(() => {
        this.getRates(true);
      });
  }

  public ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  public editRate(rate: App.ExchangeRates.Rate) {
    this.bsModalRef = this.modalService.show(RateEditDialogComponent);
    this.bsModalRef.content.rate = rate;
  }

  private getRates(isForceRefresh: boolean = false) {
    const params = isForceRefresh ? { isForceRefresh } : {};

    this.exchangeRatesService
      .getRates(params)
      .pipe(
        takeUntil(this.ngUnsubscribe)
      )
      .subscribe((rates) => {
        this.rates = rates;
      });
  }
}
