import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { SharedComponentsModule } from '../../shared/components/shared-components.module';
import { SharedPipeModule } from './../../shared/pipes/shared-pipe.module';
import { ExchangeRatesComponent } from './exchange-rates.component';
import { RateCardComponent } from './rate-card/rate-card.component';
import { RateEditDialogComponent } from './rate-edit-dialog/rate-edit-dialog.component';

export const routes: Route[] = [
  {
    path: '',
    pathMatch: 'full',
    component: ExchangeRatesComponent
  },
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedComponentsModule,
    SharedPipeModule
  ],
  declarations: [
    ExchangeRatesComponent,
    RateCardComponent,
    RateEditDialogComponent
  ],
  entryComponents: [
    RateEditDialogComponent
  ]
})
export class ExchangeRatesModule {
  public static routes = routes;
}
