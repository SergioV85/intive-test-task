import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'rate-card',
  templateUrl: './rate-card.component.html',
  styleUrls: ['./rate-card.component.scss']
})
export class RateCardComponent implements OnInit {
  @Input() public rate: App.ExchangeRates.Rate;
  @Output() public rateEdit = new EventEmitter<App.ExchangeRates.Rate>();

  constructor() { }

  public ngOnInit() {
  }

  public editRate() {
    this.rateEdit.emit(this.rate);
  }

}
