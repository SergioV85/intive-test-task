
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { takeUntil, catchError } from 'rxjs/operators';
import { _throw } from 'rxjs/observable/throw';
import { equals, merge } from 'ramda';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ExchangeRatesService } from './../../../shared/services/exchange-rates/exchange-rates.service';
import { numberOnlyValidator } from '../../../shared/validators/number-only-validator.validators';
import { finalize } from 'rxjs/operators/finalize';


@Component({
  selector: 'app-rate-edit-dialog',
  templateUrl: './rate-edit-dialog.component.html',
  styleUrls: ['./rate-edit-dialog.component.scss']
})
export class RateEditDialogComponent implements OnInit, OnDestroy {
  @Input() public set rate(rate: App.ExchangeRates.Rate) {
    this.currentRate = rate;
    this.formGroup.patchValue({
      rate: rate.Rate
    });
  }
  public formGroup: FormGroup;
  public currentRate: App.ExchangeRates.Rate;
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(public bsModalRef: BsModalRef,
              private formBuilder: FormBuilder,
              private exchangeRatesService: ExchangeRatesService) { }

  public ngOnInit() {
    this.formGroup = this.formBuilder.group({
      rate: ['', [Validators.required, numberOnlyValidator]]
    });
  }

  public ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  public controlIsInvalid(controlName: string): boolean {
    const control = this.formGroup.get(controlName);
    return control.invalid && (control.dirty || control.touched);
  }

  public controlHasErrorType(controlName: string, errorName: string) {
    const control = this.formGroup.get(controlName);
    return control.errors[errorName];
  }

  public get formIsValid() {
    return this.formGroup.invalid;
  }

  public submitForm() {
    const Rate = this.formGroup.get('rate').value;

    const newRate = merge(this.currentRate, { Rate });

    this.exchangeRatesService.updateRate(newRate)
      .pipe(
        takeUntil(this.ngUnsubscribe),
        finalize(() => {
          this.bsModalRef.hide();
        }),
        catchError((err) => {
          return _throw(err);
        })
      )
      .subscribe((value) => {
        console.log('value is', value);
      });
  }
}
