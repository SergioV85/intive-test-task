import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';

const commonModules = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule
];

@NgModule({
  imports: [
    ...commonModules,
    ModalModule.forRoot()
  ],
  declarations: [],
  exports: [
    ...commonModules
  ]
})
export class SharedComponentsModule {}
