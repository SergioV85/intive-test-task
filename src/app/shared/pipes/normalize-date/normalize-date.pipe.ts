import { Pipe, PipeTransform } from '@angular/core';
import { DateTime } from 'luxon';

@Pipe({
  name: 'normalizeDate'
})
export class NormalizeDatePipe implements PipeTransform {


  private readonly mediumWithSecFormat = 'dd MMM yyyy, HH:mm:ss';
  private readonly longerFormat = 'ccc dd MMM yyyy, HH:mm:ss';
  private readonly shortFormat = 'dd MMM yyyy';
  private readonly timeOnly = 'HH:mm:ss';

  public transform (value: string, type: string) {
    if (value) {
      const date = DateTime.fromISO(value);
      if (date.isValid) {
        switch (type) {
          case 'longer':
            return date.toFormat(this.longerFormat);
          case 'short':
            return date.toFormat(this.shortFormat);
          case 'time':
            return date.toFormat(this.timeOnly);
          default:
            return date.toFormat(this.mediumWithSecFormat);
        }
      } else {
        return value;
      }
    }
    return value;
  }

}
