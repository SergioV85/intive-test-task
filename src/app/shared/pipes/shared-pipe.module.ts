import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NormalizeDatePipe } from './normalize-date/normalize-date.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    NormalizeDatePipe
  ],
  declarations: [
    NormalizeDatePipe
  ],
  exports: [
    NormalizeDatePipe
  ]
})
export class SharedPipeModule { }
