import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ExchangeRatesService {

  constructor(private http: HttpClient) { }

  public getRates(params = {}): Observable<App.ExchangeRates.Rate[]> {
    return this.http.get<App.ExchangeRates.Rate[]>('http://www.orienteeringorganiser.com/api/exchangeRates', { params });
  }

  public updateRate(rate: App.ExchangeRates.Rate) {
    return this.http.put<App.ExchangeRates.Rate>('http://www.orienteeringorganiser.com/api/exchangeRates', rate);
  }
}
