import { TestBed, inject } from '@angular/core/testing';

import { HttpCacheInterceptorService } from './http-cache-interceptor.service';

describe('HttpCacheInterceptorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HttpCacheInterceptorService]
    });
  });

  it('should be created', inject([HttpCacheInterceptorService], (service: HttpCacheInterceptorService) => {
    expect(service).toBeTruthy();
  }));
});
