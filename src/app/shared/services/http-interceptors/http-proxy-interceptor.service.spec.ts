import { TestBed, inject } from '@angular/core/testing';

import { HttpProxyInterceptorService } from './http-proxy-interceptor.service';

describe('HttpProxyInterceptorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HttpProxyInterceptorService]
    });
  });

  it('should be created', inject([HttpProxyInterceptorService], (service: HttpProxyInterceptorService) => {
    expect(service).toBeTruthy();
  }));
});
