// tslint:disable:no-any
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { environment } from './../../../../environments/environment';

@Injectable()
export class HttpProxyInterceptorService {

  constructor() { }

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const url = `${environment.proxyUrl}${req.url}`;
    const modifiedRequest = req.clone({ url });
    return next.handle(modifiedRequest);
  }
}
