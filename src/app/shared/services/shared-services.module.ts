import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ExchangeRatesService } from './exchange-rates/exchange-rates.service';
import { HttpCacheInterceptorService } from './http-interceptors/http-cache-interceptor.service';
import { HttpProxyInterceptorService } from './http-interceptors/http-proxy-interceptor.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpCacheInterceptorService,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpProxyInterceptorService,
      multi: true,
    },
    ExchangeRatesService
  ],
  declarations: []
})
export class SharedServicesModule {
  public static forRoot() {
    return {
      ngModule: SharedServicesModule
    };
  }
}
