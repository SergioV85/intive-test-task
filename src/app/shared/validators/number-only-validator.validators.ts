import { ValidatorFn, AbstractControl, FormControl } from '@angular/forms';
import { isNil, isEmpty } from 'ramda';

export function numberOnlyValidator(control: FormControl) {
  const value = control.value;
  const numberRegExp = new RegExp(/^\d+(\.?|\,?)\d*$/g);

  const match = !isNil(value) && !isEmpty(value) && !numberRegExp.test(value);
  return match ? {'numberOnly': { value }} : null;
}
