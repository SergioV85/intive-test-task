declare namespace App {
  namespace ExchangeRates {
    interface Response {
      
    }

    interface Rate {
      CurrencyPair: string;
      Date: string;
      Id: number;
      Rate: number;
    }
  }
}